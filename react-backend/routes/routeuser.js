var express = require('express');
var router = express.Router();
const { sha256 } = require('crypto-hash');
let User = require('../src/database/models/user');

router.get("/isLogged", function (req, res) {
    console.log(req.session.key, 'in the login ');
    if (req.session.key) {
        res.status(200).end()
    } else {
        var err = new Error("Not logged in!");
        res.status(400).end()
    }
});
 
router.get("/logout", function (req, res) {
    req.session.destroy();
    res.end("destroyed");    
});

router.post("/", (req, res, next) => {
    if (req.body.action == "login") {
        (async () => {
            pass = await sha256(req.body.password)
            User.findOne({ email: req.body.email })
                .then(document => {
                    if (document) {
                        if (pass === document.password) {
                            req.session.key = document._id;
                            req.session.name = document.email;
                            res.json({ status: 200, message: 'Logged in', Userdata: document });
                           
                            console.log('you created a session ', req.session);
                        }
                        else console.log("incorrect");
                    }
                    else console.log("incorrect");

                });
        })();
    }
    else {
        (async () => {
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: await sha256(req.body.password)
            });
            newUser.save().then(document => {
                res.json({ state: true, msg: "data inserted successully", document: document })
            }).catch(err => {
                res.send(err);
            });
        })();
    }

});

router.get("/", function (req, res) {
    var name = '';
    if (req.query.firstName != undefined)
        name = req.query.firstName;
    User.find({ name: { $regex: '.*' + name + '.*' } })
        .skip(parseInt(req.query.skip))
        .limit(parseInt(req.query.limit))
        .then(documents => {
            res.json({ status: 200, message: 'Users data fetched successfully', Userdata: documents });
        });
});

router.get("/check", function (req, res) {
    if (req.session.key)
        res.json({ status: 200, message: 'User is connected', Userdata: true });
    else res.json({ status: 400, message: 'User is disconnected', Userdata: false });
});




router.put('/:id', (req, res, next) => {
    const newuser = { _id: req.params.id };
    (async () => {
        User.updateOne(newuser, {
            name: req.body.name,
            email: req.body.email,
            password: await sha256(req.body.password)
        }).then(document => {
            if (!document) {
                return res.st(404).end();
            }
            return res.status(200).json(document);
        })
            .catch(err => next(err));
    })();
    User.updateOne(newuser, {
        name: req.body.name,
        email: req.body.email,
    }).then(document => {
        if (!document) {
            return res.st(404).end();
        }
        return res.status(200).json(document);
    })
        .catch(err => next(err));
});

router.get('/:id', function (req, res) {
    User.find({ _id: req.params.id })
        .then(documents => {
            res.json({ status: 200, message: 'Users data fetched successfully', Userdata: documents });
        });
});

router.delete('/:id', (req, res, next) => {
    User.deleteOne({ _id: req.params.id }).then(document => {
        res.json({ status: 200, message: 'Users data deleted Successfully', document: document });
    })
        .catch(err => next(err));
});

module.exports = router;