import './App.css';
import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
import UsersTable from './components/UsersTable';
import Login from './components/Login';
import Home from './components/Home';
import Logout from './components/Logout';
import { Router, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav } from 'react-bootstrap';
import axios from './utils/axios';
let history = createBrowserHistory();



const styles = {
  content: {
    display: 'flex',
    flexDirection: 'column'

  },
};

class App extends Component {
  state = {
    isLogged: false
  }

  componentDidMount() {
    axios.get('user/isLogged/').then((res) => {
      console.log(res.status);
      this.setState({ isLogged: true });
    }).catch(() => {
      this.setState({ isLogged: false });
    });
  }

  render() {
    console.log("Logged in", this.state.isLogged);
    if (this.state.isLogged) {
      return (
        <div className="App" style={{ width: '100%', alignItems: 'center' }}>
          <Router history={history}>
            <Navbar bg="primary" variant="dark">
              <Navbar.Brand href="#home">Intern's App</Navbar.Brand>
              <Nav className="mr-auto">
                <Nav.Link href="/home">Home</Nav.Link>
                <Nav.Link href="/users">Users</Nav.Link>
                <Nav.Link href="/logout">Logout</Nav.Link>
              </Nav>
            </Navbar>
            <Route path="/home" component={Home} />
            <Route path="/logout" component={Logout} />
            <Route path="/users" component={UsersTable} />
          </Router>
        </div>
      );
    }

    else {
      return (
        <div className="App" style={{ width: '100%', alignItems: 'center' }}>
          <Router history={history}>
            <Navbar bg="primary" variant="dark">
              <Navbar.Brand href="#home">Intern's App</Navbar.Brand>
              <Nav className="mr-auto">
                <Nav.Link href="/home">Home</Nav.Link>
                <Nav.Link href="/users">Users</Nav.Link>
                <Nav.Link href="/login">Login</Nav.Link>
              </Nav>
            </Navbar>
            <Route path="/home" component={Home} />
            <Route path="/login" component={Login} />
            <Route path="/users" component={UsersTable} />
          </Router>
        </div>
      );
    }
  }
}

export default withStyles(styles)(App);