import { withStyles } from '@material-ui/core/styles';
import React, { Component } from 'react';
import axios from '../utils/axios';

const styles = {
  content: {
    display: 'flex',
    flexDirection: 'column'

  }
};

class Logout extends Component {




  componentDidMount() {
    axios.get('user/logout')
      .then(res => {
        console.log(res);
      });
  }

  render() {


    return (
      <div style={{ width: '100%', alignItems: 'center' }}>
        <hr></hr>
        <h1>You just logged out. You can still visit the homepage tho'.</h1>
        <hr></hr>
        <h3>SIMPLE!</h3>
      </div>
    );
  }
}

export default withStyles(styles)(Logout);