import './Login.css';
import React, { Component } from "react";
import axios from '../utils/axios';
import { withStyles } from '@material-ui/core/styles';
import { Form, Button } from 'react-bootstrap';


const styles = {
  content: {
    display: 'flex',
    flexDirection: 'column'

  }
};

class Login extends Component {
  state = {
    email: '',
    password: ''
  }



  componentDidMount() {

  }

  render() {

    const handleSubmit = () => {
      axios.post('user', {
        email: this.state.email,
        password: this.state.password,
        action: "login"
      })
        .then((res) => {
          console.log(res);
        }, (error) => {
          console.log(error);
        });
    }

    const validateForm = () => {
      return this.state.email.length > 0 && this.state.password.length > 0;
    }

    const setPassword = (value) => {
      this.setState({ password: value });
    }

    const setEmail = (value) => {
      this.setState({ email: value });
    }

    return (
      <div className="Login" style={{ width: '100%', alignItems: 'center' }}>
       
          <Form.Group size="lg" controlId="email">
            <Form.Label>Email</Form.Label>
            <Form.Control
              autoFocus
              type="text"
              value={this.state.email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group size="lg" controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              value={this.state.password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>
          <Button onClick={handleSubmit} block size="lg" type="submit" disabled={!validateForm()}>
            Login
        </Button>
        
      </div>
    );
  }
}

export default withStyles(styles)(Login);