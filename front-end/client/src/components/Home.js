import { withStyles } from '@material-ui/core/styles';
import React, { Component } from 'react';

const styles = {
  content: {
    display: 'flex',
    flexDirection: 'column'

  }
};

class Home extends Component {
  



  componentDidMount() {
    
  }

  render() {


    return (
      <div style={{ width: '100%', alignItems: 'center' }}>
        <hr></hr>
        <h1>Hello! This is the homepage!</h1>
        <hr></hr>
        <h3>This is a simple homepage for a simple aplication with simple functionality.</h3>
        <h3> You simply press the simple buttons and it just works.</h3>
        <h3>SIMPLE!</h3>
      </div>
    );
  }
}

export default withStyles(styles)(Home);