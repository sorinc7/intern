import React, { Component } from "react";
import axios from '../utils/axios';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import { TableBody, Table, TextField, TablePagination, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


const styles = {
  content: {
    display: 'flex',
    flexDirection: 'column'

  }
};

class UsersTable extends Component {
  state = {
    users: [],
    totalUsers: 0,
    page: 0,
    rowsPerPage: 5,
    open: false,
    labelName: 'Insert name',
    labelEmail: 'Insert email',
    labelPassword: 'Insert password',
    idToModify: 'undefined',
    userText: '',
    emailText: '',
    passwordText: '',
    searchInput: ''
  }



  componentDidMount() {
    axios.get('user?limit=' + this.state.rowsPerPage)
      .then(res => {
        const users = res.data.Userdata;
        this.setState({ users });
      }).catch(err => {
        console.log(err)
      });
    axios.get('user')
      .then(res => {
        const totalUsers = res.data.Userdata.length;
        this.setState({ totalUsers });
      }).catch(err => {
        console.log(err)
      });
  }

  deleteUser(id) {
    var message = "Do you really want to delete this user?";
    if (window.confirm(message)) {
      axios.delete('user/' + id);
      axios.get('user?limit=' + this.state.rowsPerPage)
        .then(res => {
          const users = res.data.Userdata;
          this.setState({ users });
        });
    }
  }

  

  queryTable(event) {
    let search = event ? event.target.value : ''
    this.setState({ searchInput: search });
    axios.get('user?firstName=' + search + '&skip=' + this.state.page + '&limit=' + this.state.rowsPerPage)
      .then(res => {
        const users = res.data.Userdata;
        this.setState({ users });
      });
    axios.get('user?firstName=' + search)
      .then(res => {
        const totalUsers = res.data.Userdata.length;
        this.setState({ totalUsers });
      });
  }

  userText(event) {
    this.setState({userText: event.target.value});
  }

  emailText(event) {
    this.setState({ emailText: event.target.value });
  }

  passwordText(event) {
    this.setState({ passwordText: event.target.value });
  }

  modifyData = () => {
    if(this.state.idToModify === 'undefined'){
      axios.post('user/', {
          name: this.state.userText,
          email: this.state.emailText,
          password: this.state.passwordText,
          action: "create"
        })
        .then((response) => {
          console.log(response);
          console.log("Password to send" + this.state.passwordText);
        }, (error) => {
          console.log(error);
        });
        this.setState({ open: false, labelName: 'Insert name', labelEmail: 'Insert email', labelPassword: 'Insert password', idToModify: 'undefined', totalUsers: this.state.totalUsers + 1 });
    }
    else {
      axios.put('user/' + this.state.idToModify, { name: this.state.userText, email: this.state.emailText, password: this.state.passwordText, action: "modify" })
      .then(() => { 
        this.setState({ open: false }) 
        this.queryTable()
      });
      this.setState({ open: false, labelName: 'Insert name', labelEmail: 'Insert email', labelPassword: 'Insert password', idToModify: 'undefined' });

    }
  }

  render() {
    const handleOpen = (id) => {
      if(id==null){
        this.setState({ open: true });
      }
      else{
        this.setState({ open: true });
        axios.get('user/' + id)
        .then(res => {
          this.setState({labelName: res.data.Userdata[0].name, labelEmail: res.data.Userdata[0].email});
        });
        this.setState({ idToModify: id });
      }
    };

    const handleClose = () => {
      this.setState({ open: false, labelName: 'Insert name', labelEmail: 'Insert email', labelPassword: 'Insert password', idToModify: 'undefined' });
    };

    const handleChangePage = (_, page) => {

      this.setState({ page });
      axios.get('user?firstName=' + this.state.searchInput + '&skip=' + this.state.rowsPerPage * page + '&limit=' + this.state.rowsPerPage)
        .then(res => {
          const users = res.data.Userdata;
          this.setState({ users });
        });
    };

    const handleChangeRowsPerPage = (event) => {
      this.setState({ rowsPerPage: event.target.value });
    };

    return (
      <div className="App" style={{ width: '100%', alignItems: 'center' }}>
        <h1>Users table</h1>
        <Table>
          <TableBody style={{ width: '50%', margin: "auto" }}>
            <TableRow>
              <TableCell>
                  <Button variant="outlined" onClick={() => handleOpen(null)} color="primary">
                    CREATE USER
                  </Button>
              </TableCell>
              <TableCell></TableCell>
              <TableCell></TableCell>
              <TableCell><TextField id="searchInput" onChange={event => this.queryTable(event)} label="Query here" style={{ float: 'right' }}></TextField></TableCell>
            </TableRow>
            {this.state.users.map(user =>
              <TableRow key={user._id}>
                <TableCell>
                  {user.name}
                </TableCell>
                <TableCell>
                  {user.email}
                </TableCell>
                <TableCell>
                  {user.password}
                </TableCell>
                <TableCell>
                  <Button variant="outlined" onClick={() => handleOpen(user._id)} color="primary" style={{ float: 'right' }}>
                    EDIT
                  </Button>
                  <Button variant="outlined" onClick={() => this.deleteUser(user._id)} color="secondary" style={{ float: 'right', marginRight: '10px' }}>
                    X
                  </Button>
                </TableCell>
              </TableRow>

            )}

          </TableBody>
        </Table>
        <TablePagination
          rowsPerPageOptions={[5]}
          component="div"
          count={this.state.totalUsers}
          rowsPerPage={this.state.rowsPerPage}
          page={this.state.page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
        <Dialog onClose={handleClose} open={this.state.open} fullWidth = { true } maxWidth="lg">
          <DialogTitle>Edit user account</DialogTitle>
          <DialogContent className={this.props.classes.content}>
            <TextField id="username" onChange={event => this.userText(event)} label={ this.state.labelName } style={{ color: "white" }}></TextField>
            <TextField id="email" onChange={event => this.emailText(event)} label={ this.state.labelEmail } style={{ textDecorationColor: 'white' }}></TextField>
            <TextField id="password" onChange={event => this.passwordText(event)} label={ this.state.labelPassword } style={{ textDecorationColor: 'white' }}></TextField>
          </DialogContent>
          <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={() =>{ this.modifyData() }} color="primary">
            Send
          </Button>
        </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(UsersTable);